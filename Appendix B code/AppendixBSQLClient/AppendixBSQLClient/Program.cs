﻿using AppendixBSQLClient.DataAccess;
using AppendixBSQLClient.Models;
using System;
using System.Collections.Generic;

namespace AppendixBSQLClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            //Remove the comment from any of these steps to see them in action
            //TestSelectAll(repository);        //Step 1
            //TestSelectByNumber(repository);   //Step 2
            //TestSelectByName(repository);     //Step 3
            //TestSelectRange(repository);      //Step 4
            //TestInsert(repository);           //Step 5
            //TestUpdate(repository);           //Step 6
            //TestSelectByCountry(repository);  //Step 7
            //TestSelectBySpending(repository); //Step 8
            //TestSelectByGenre(repository);    //Step 9
        }

        /// <summary>
        /// The 9 TestSelect methods invoke the repository retrieval methods in CustomerRepository.cs for each Step in the assignment, then invoke printing methods
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSelectByNumber(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByNumber(10));
        }

        static void TestSelectByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("Kara", "Nielsen"));
        }

        static void TestSelectRange(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomersByRange(20, 10));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer insertedCustomer = new Customer()
            {
                FirstName = "Hendrik",
                LastName = "Zwart",
                Country = "Netherlands", //Nullable
                PostalCode = "3011", //Nullable
                Phone = "+31 06 52361824", //Nullable
                Email = "henkzwart@gmail.com"
            };
            if(repository.AddNewCustomer(insertedCustomer))
            {
                Console.WriteLine("We did it!");
                PrintCustomer(repository.GetCustomerByName(insertedCustomer.FirstName, insertedCustomer.LastName));
            } else
            {
                Console.WriteLine("That's not supposted to happen!");
            }
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer updatedCustomer = new Customer()
            {
                CustomerId = 60,
                FirstName = "Steve",
                LastName = "Black",
                Country = "USA", //Nullable
                PostalCode = "2113", //Nullable
                Phone = "+1 (394) 756-1234", //Nullable
                Email = "steveblack@gmail.com"
            };
            if (repository.UpdateCustomer(updatedCustomer))
            {
                Console.WriteLine("We did it!");
                PrintCustomer(repository.GetCustomerByNumber(updatedCustomer.CustomerId));
            }
            else
            {
                Console.WriteLine("That's not supposted to happen!");
            }
        }

        static void TestSelectByCountry(ICustomerRepository repository)
        {
            PrintCountries(repository.GetCustomerCountries());
        }

        static void TestSelectBySpending(ICustomerRepository repository)
        {
            PrintSpenders(repository.GetCustomerSpenders());
        }

        static void TestSelectByGenre(ICustomerRepository repository)
        {
            PrintGenres(repository.GetCustomerGenre(12));
        }
        /// <summary>
        /// End of the TestSelect methods
        /// </summary>
        /// <param name="customers"></param>


        /// <summary>
        /// Print information according to the Customer Model(Step 1-6)
        /// </summary>
        /// <param name="customer"></param>
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"-- { customer.CustomerId } { customer.FirstName } { customer.LastName } { customer.Country } { customer.PostalCode } { customer.Phone } { customer.Email } ---");
        }
        /// <summary>
        /// End of Customer Model
        /// </summary>


        /// <summary>
        /// Print information according to the CustomerCountry Model(Step 7)
        /// </summary>
        /// <param name="customerCountries"></param>
        static void PrintCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCountry(customerCountry);
            }
        }

        static void PrintCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"-- { customerCountry.Country } has { customerCountry.CountryAmount } customers ---");
        }
        /// <summary>
        /// End of CustomerCountry Model
        /// </summary>


        /// <summary>
        /// Print information according to the CustomerSpender Model(Step 8)
        /// </summary>
        /// <param name="customerSpenders"></param>
        static void PrintSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                PrintSpender(customerSpender);
            }
        }

        static void PrintSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"-- { customerSpender.FirstName } { customerSpender.LastName } has spent { customerSpender.Total } ---");
        }
        /// <summary>
        /// End of CustomerSpender Model
        /// </summary>


        /// <summary>
        /// Print information according to the CustomerGenre Model(Step 9)
        /// </summary>
        /// <param name="customerGenres"></param>
        static void PrintGenres(IEnumerable<CustomerGenre> customerGenres)
        {
            int maxSongs = 0;
            foreach (CustomerGenre customerGenre in customerGenres)
            {
                if(customerGenre.SongsInGenre >= maxSongs)
                {
                    maxSongs = customerGenre.SongsInGenre;
                    PrintGenre(customerGenre);
                }
            }
        }

        static void PrintGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"-- { customerGenre.FirstName } { customerGenre.LastName } likes { customerGenre.Genre } music (has { customerGenre.SongsInGenre } songs) ---");   
        }
        /// <summary>
        /// End of CustomerGenre Model
        /// </summary>
    }
}
