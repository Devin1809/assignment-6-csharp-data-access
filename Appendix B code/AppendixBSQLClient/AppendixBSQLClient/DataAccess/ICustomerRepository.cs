﻿using AppendixBSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixBSQLClient.DataAccess
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Selects the CustomerId, FirstName, LastName, Country, PostalCode, Phone and Email from all Customers
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAllCustomers();

        /// <summary>
        /// Gets one Customer by their CustomerId (might break if the given CustomerId does not exists in the database)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customer GetCustomerByNumber(int id);

        /// <summary>
        /// Gets a Customer by their full name. This ensures there won't be multiple matching Customers (might break on identical full names in the database)
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public Customer GetCustomerByName(string firstName, string lastName);

        /// <summary>
        /// Gets all the customers within the specified offset and limit (might break if it tries retrieving CustomerId's that don't exist)
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public List<Customer> GetCustomersByRange(int offset, int limit);

        /// <summary>
        /// Is used to create a new Customer from scratch (the nullable columns are commented in the Program.cs file when creating)
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool AddNewCustomer(Customer customer);

        /// <summary>
        /// Does the same as AddNewCustomer(), except it updates an existing Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer);

        /// <summary>
        /// Returns a descending list of Countries by the amount of Customers
        /// </summary>
        /// <returns></returns>
        public List<CustomerCountry> GetCustomerCountries();

        /// <summary>
        /// Returns a descending list of Customers by the total amount they spent
        /// </summary>
        /// <returns></returns>
        public List<CustomerSpender> GetCustomerSpenders();

        /// <summary>
        /// Gets one Customers' favorite Genre of music and how many songs they bought for it (displays multiple genres if it's a tie)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<CustomerGenre> GetCustomerGenre(int id);
    }
}
