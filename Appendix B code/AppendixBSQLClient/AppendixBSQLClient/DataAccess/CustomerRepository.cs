﻿using AppendixBSQLClient.Models;
using AppendixBSQLClient.Repositories;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixBSQLClient.DataAccess
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// See the ICustomerRepository.cs file for the method summaries
        /// </summary>

        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }
                                else
                                {
                                    temp.Country = null;
                                }

                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                else
                                {
                                    temp.PostalCode = null;
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    temp.Phone = reader.GetString(5);
                                }
                                else
                                {
                                    temp.Phone = null;
                                }

                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            } catch(SqlException ex)
            {

            }
            return custList;
        }

        public Customer GetCustomerByNumber(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
            " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);
                                }
                                else
                                {
                                    customer.Country = null;
                                }

                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);
                                }
                                else
                                {
                                    customer.PostalCode = null;
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    customer.Phone = reader.GetString(5);
                                }
                                else
                                {
                                    customer.Phone = null;
                                }

                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            } catch(SqlException ex)
            {

            }
            return customer;
        }

        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
            " WHERE FirstName = @FirstName AND LastName = @LastName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", firstName);
                        cmd.Parameters.AddWithValue("@LastName", lastName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);
                                }
                                else
                                {
                                    customer.Country = null;
                                }

                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);
                                }
                                else
                                {
                                    customer.PostalCode = null;
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    customer.Phone = reader.GetString(5);
                                }
                                else
                                {
                                    customer.Phone = null;
                                }

                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return customer;
        }

        public List<Customer> GetCustomersByRange(int offset, int limit)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId BETWEEN @offset AND @limit";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit + offset);
                        cmd.Parameters.AddWithValue("@offset", offset +1);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }
                                else
                                {
                                    temp.Country = null;
                                }

                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                else
                                {
                                    temp.PostalCode = null;
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    temp.Phone = reader.GetString(5);
                                }
                                else
                                {
                                    temp.Phone = null;
                                }

                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return custList;
        }

        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            } catch (SqlException ex)
            {

            }
            return success;
        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET Firstname = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return success;
        }

        public List<CustomerCountry> GetCustomerCountries()
        {
            List<CustomerCountry> countryList = new List<CustomerCountry>();

            string sql = "SELECT Country, COUNT(*) AS CountryAmount" +
                " FROM Customer" +
                " GROUP BY Country ORDER BY CountryAmount DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                if (!reader.IsDBNull(0))
                                {
                                    temp.Country = reader.GetString(0);
                                }
                                else
                                {
                                    temp.Country = null;
                                }
                                temp.CountryAmount = reader.GetInt32(1);
                                countryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return countryList;
        }

        public List<CustomerSpender> GetCustomerSpenders()
        {
            List<CustomerSpender> spenderList = new List<CustomerSpender>();

            string sql = "SELECT Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS Total" +
                " FROM Customer" +
                " JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId" +
                " GROUP BY Customer.FirstName, Customer.LastName ORDER BY Total DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                if (!reader.IsDBNull(0))
                                {
                                    temp.FirstName = reader.GetString(0);
                                }
                                else
                                {
                                    temp.FirstName = null;
                                }
                                if (!reader.IsDBNull(1))
                                {
                                    temp.LastName = reader.GetString(1);
                                }
                                else
                                {
                                    temp.LastName = null;
                                }
                                temp.Total = reader.GetDecimal(2);
                                spenderList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return spenderList;
        }

        public List<CustomerGenre> GetCustomerGenre(int id)
        {
            List<CustomerGenre> genreList = new List<CustomerGenre>();
            string sql = "SELECT Customer.FirstName, Customer.LastName, Genre.Name, Count(Genre.Name) AS SongsInGenre" +
                " FROM Customer" +
                " JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId" +
                " JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId" +
                " JOIN Track ON InvoiceLine.TrackId = Track.TrackId" +
                " JOIN Genre ON Track.GenreId = Genre.GenreId" +
                " WHERE Customer.CustomerId = @CustomerId" +
                " GROUP BY Customer.FirstName, Customer.LastName, Genre.Name" +
                " ORDER BY SongsInGenre DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                if (!reader.IsDBNull(0))
                                {
                                    temp.FirstName = reader.GetString(0);
                                }
                                else
                                {
                                    temp.FirstName = null;
                                }
                                if (!reader.IsDBNull(1))
                                {
                                    temp.LastName = reader.GetString(1);
                                }
                                else
                                {
                                    temp.LastName = null;
                                }
                                temp.Genre = reader.GetString(2);
                                temp.SongsInGenre = reader.GetInt32(3);
                                genreList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return genreList;
        }
    }
}
