CREATE TABLE LinkSuperheroSuperpower
(
	Id INTEGER PRIMARY KEY IDENTITY(1,1)
)

ALTER TABLE LinkSuperheroSuperPower
	ADD SuperheroId int CONSTRAINT FK_LinkSuperheroSuperpower_Superhero FOREIGN KEY REFERENCES Superhero(Id)

ALTER TABLE LinkSuperheroSuperPower
	ADD SuperpowerId int CONSTRAINT FK_LinkSuperheroSuperpower_Superpower FOREIGN KEY REFERENCES Superpower(Id)