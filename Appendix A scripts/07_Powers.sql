INSERT INTO Superpower (Powername, Explanation)
	VALUES ('X-ray', 'Can see through most things');

INSERT INTO Superpower (Powername, Explanation)
	VALUES ('Flight', 'Can fly with their body');

INSERT INTO Superpower (Powername, Explanation)
	VALUES ('Invisible', 'Can become invisible');

INSERT INTO Superpower (Powername, Explanation)
	VALUES ('Radioactive', 'Can project radiation');


INSERT INTO LinkSuperheroSuperpower (SuperheroId, SuperpowerId)
	VALUES (1, 1);

INSERT INTO LinkSuperheroSuperpower (SuperheroId, SuperpowerId)
	VALUES (1, 2);

INSERT INTO LinkSuperheroSuperpower (SuperheroId, SuperpowerId)
	VALUES (1, 3);

INSERT INTO LinkSuperheroSuperpower (SuperheroId, SuperpowerId)
	VALUES (2, 4);

INSERT INTO LinkSuperheroSuperpower (SuperheroId, SuperpowerId)
	VALUES (3, 4);