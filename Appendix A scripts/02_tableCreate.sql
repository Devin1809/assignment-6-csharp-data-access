CREATE TABLE Superhero 
(
	Id INTEGER PRIMARY KEY IDENTITY(1,1),
	PersonName nvarchar(50) NOT NULL,
	Alias nvarchar(50) NOT NULL,
	Origin nvarchar(50) NOT NULL
)

CREATE TABLE Assistant
(
	Id INTEGER PRIMARY KEY IDENTITY(1,1),
	PersonName nvarchar(50) NOT NULL
)

CREATE TABLE Superpower
(
	Id INTEGER PRIMARY KEY IDENTITY(1,1),
	Powername nvarchar(50) NOT NULL,
	Explanation nvarchar(100) NOT NULL
)