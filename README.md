**Week 6 assignment**

For the .NET Full Stack Noroff course we had to create a database using SQL scripts. These scripts are featured in the "Appendix A" folder. For the second part of the assignment we had to read data from a database using the SQL Client. This code is featured in Appendix B.


**Appendix A:**

Several scripts were written to:
- create a database
- Setup tables
- Add relationships
- Popoluate the tables with some data (Create, Update, Delete)


**Appendix B:**

For this part of the assignment the database "Chinook" was used. Chinook contains data about a music streaming service (tracks, genres, invoice, customer, playlists). The SQL Client library was used to manipulate SQL Server Data in Visual Studio.

The following functionalities were added:
- Read all customers
- Read specific customer (by ID)
- Read specific customer (by Name)
- Return page of customers (using offset and limit)
- Add new customer
- Update customer
- Return number of customers in each country (order descending)
- Highest spending customers (order descending)
- For given customer, most popular genre
